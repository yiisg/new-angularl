import { TestBed, async } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { StorageService } from '../core/services/storage.service';
import { Routing } from '../app.routing';
import { AuthenticationService } from '../login/shared/authentication.service';
import { LoginComponent } from '../login/login.component';
import { MatCardModule, MatFormFieldModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
describe('AppComponent', () => {
    const userdata= {
    user:{
      email: "jcast@somewhere.com(se abre en una pestaña nueva)",
      id: 1,
      name: "Javier",
      password: "abcd",
      surname: "Castillo",
      username: "jcastillo"
    }
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
        HomeComponent
      ],
      imports: [
        MatCardModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        Routing
      ],
      providers: [
        StorageService,
        AuthenticationService,
        HttpClient,
        HttpHandler
      ]
    }).compileComponents();
  }));
  it('Saludo contiene el nombre correctamente', async(() => {
    const fixture_Home = TestBed.createComponent(HomeComponent);                  //crear componente
    const home = fixture_Home.componentInstance;                                  //instancia del componente
    home.user = userdata.user;                                                    //le asignamos nuestro usuario de beforeeach
    console.log("USER.NAME EN HOME TIENE EL VALOR DE -> " + home.user.name);      //comprobamos el valor de home
    home.welcomeText = home.saludar();                                            //asignamos el saludo
    console.log("EL MENSAJE DE BIENVENIDA EN HOME AHORA ES -> " + home.welcomeText);//comprobamos el mensaje de saludo
    expect(home.welcomeText).toContain(userdata.user.name);                       //verificamos con expect.
  }));
});
